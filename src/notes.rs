use core::{
    borrow::Borrow,
    ops::{Add, Sub},
};

#[derive(Debug, Copy, Clone, PartialEq)]
/// The seven notes
pub enum Note {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
}

#[derive(Debug, Copy, Clone, PartialEq)]
/// Accidentals (sharp, flat,...)
pub enum Accidental {
    Sharp,
    Flat,
    Natural,
    DoubleFlat,
    DoubleSharp,
}

impl PartialEq for MusicalNote {
    fn eq(&self, other: &Self) -> bool {
        SemitonesFromC::from(self).0 == SemitonesFromC::from(other).0
    }
}

#[derive(Debug, Copy, Clone)]
/// The distance (in semitones) from two notes.
pub struct Interval(pub i8);

impl From<i8> for Interval {
    fn from(i: i8) -> Self {
        Self(i)
    }
}

#[derive(Debug, Copy, Clone)]
/// A note (one of the twelve piano keys)
pub struct MusicalNote(pub Note, pub Accidental);

impl Sub for MusicalNote {
    type Output = Interval;

    fn sub(self, rhs: Self) -> Self::Output {
        Interval(SemitonesFromC::from(self) - SemitonesFromC::from(rhs))
    }
}

impl<T> Add<T> for MusicalNote
where
    T: Into<Interval>,
{
    type Output = Self;

    fn add(self, rhs: T) -> Self::Output {
        Self::from(SemitonesFromC(
            (SemitonesFromC::from(self).0 + rhs.into().0) % 12,
        ))
    }
}

impl From<(Note, Accidental)> for MusicalNote {
    fn from(n: (Note, Accidental)) -> Self {
        Self(n.0, n.1)
    }
}

impl MusicalNote {
    pub fn get_frequency(&self, octave: i8) -> f32 {
        let a: f32 = micromath::F32Ext::powf(2f32, 1.0 / 12.0);
        let a4 = SemitonesFromC::from(MusicalNote(Note::A, Accidental::Natural)).0 + 4 * 12;
        440.0
            * micromath::F32Ext::powf(
                a,
                ((octave * 12 + SemitonesFromC::from(self).0) - a4).into(),
            )
    }
}

/// The number of semitones of C (used for calculations).
struct SemitonesFromC(i8);

impl Sub for SemitonesFromC {
    type Output = i8;

    fn sub(self, rhs: Self) -> Self::Output {
        self.0 - rhs.0
    }
}

impl<N: Borrow<MusicalNote>> From<N> for SemitonesFromC {
    fn from(n: N) -> Self {
        let (note, accidental) = (n.borrow().0, n.borrow().1);
        let number = match note {
            Note::A => 9,
            Note::B => 11,
            Note::C => 0,
            Note::D => 2,
            Note::E => 4,
            Note::F => 5,
            Note::G => 7,
        };
        let delta = match accidental {
            Accidental::Sharp => 1,
            Accidental::Flat => -1,
            Accidental::Natural => 0,
            Accidental::DoubleFlat => -2,
            Accidental::DoubleSharp => 2,
        };
        SemitonesFromC(number + delta)
    }
}
impl From<SemitonesFromC> for MusicalNote {
    fn from(n: SemitonesFromC) -> Self {
        match n.0 % 12 {
            0 => (Note::C, Accidental::Natural),
            1 => (Note::D, Accidental::Flat),
            2 => (Note::D, Accidental::Natural),
            3 => (Note::E, Accidental::Flat),
            4 => (Note::E, Accidental::Natural),
            5 => (Note::F, Accidental::Natural),
            6 => (Note::F, Accidental::Sharp),
            7 => (Note::G, Accidental::Natural),
            8 => (Note::A, Accidental::Flat),
            9 => (Note::A, Accidental::Natural),
            10 => (Note::B, Accidental::Flat),
            11 => (Note::B, Accidental::Natural),
            _ => panic!("Unreachable code reached"),
        }
        .into()
    }
}
