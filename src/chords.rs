use core::{borrow::Borrow, fmt::Display};

use crate::{chord_type, notes::MusicalNote, Mode};

#[derive(Debug, Copy, Clone, PartialEq)]
/// The various chords this crate can generate
pub enum Chord {
    Triad([MusicalNote; 3]),
    Seventh([MusicalNote; 4]),
    FiveNotesChord([MusicalNote; 5]),
    SixNotesChord([MusicalNote; 6]),
    SevenNotesChord([MusicalNote; 7]),
}
impl From<[MusicalNote; 3]> for Chord {
    fn from(c: [MusicalNote; 3]) -> Self {
        Self::Triad(c)
    }
}
impl From<[MusicalNote; 4]> for Chord {
    fn from(c: [MusicalNote; 4]) -> Self {
        Self::Seventh(c)
    }
}
impl From<[MusicalNote; 5]> for Chord {
    fn from(c: [MusicalNote; 5]) -> Self {
        Self::FiveNotesChord(c)
    }
}
impl From<[MusicalNote; 6]> for Chord {
    fn from(c: [MusicalNote; 6]) -> Self {
        Self::SixNotesChord(c)
    }
}
impl From<[MusicalNote; 7]> for Chord {
    fn from(c: [MusicalNote; 7]) -> Self {
        Self::SevenNotesChord(c)
    }
}

impl<M: Mode> From<(MusicalNote, M)> for Chord {
    fn from(n: (MusicalNote, M)) -> Self {
        let (tonality, mode) = n;
        mode.make_chord(tonality)
    }
}

impl Borrow<[MusicalNote]> for Chord {
    fn borrow(&self) -> &[MusicalNote] {
        match self {
            Chord::Triad(x) => x,
            Chord::Seventh(x) => x,
            Chord::FiveNotesChord(x) => x,
            Chord::SixNotesChord(x) => x,
            Chord::SevenNotesChord(x) => x,
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum ChordType {
    Triad(chord_type::triad::Mode),
    Seventh(chord_type::seventh::Mode),
    Suspended(chord_type::suspended::Mode),
    Extended(chord_type::extended::Mode),
}
impl From<chord_type::triad::Mode> for ChordType {
    fn from(c: chord_type::triad::Mode) -> Self {
        Self::Triad(c)
    }
}
impl From<chord_type::seventh::Mode> for ChordType {
    fn from(c: chord_type::seventh::Mode) -> Self {
        Self::Seventh(c)
    }
}
impl From<chord_type::suspended::Mode> for ChordType {
    fn from(c: chord_type::suspended::Mode) -> Self {
        Self::Suspended(c)
    }
}
impl From<chord_type::extended::Mode> for ChordType {
    fn from(c: chord_type::extended::Mode) -> Self {
        Self::Extended(c)
    }
}
impl Mode for ChordType {
    fn make_chord(&self, n: MusicalNote) -> Chord {
        match self {
            ChordType::Triad(m) => m.make_chord(n),
            ChordType::Seventh(m) => m.make_chord(n),
            ChordType::Suspended(m) => m.make_chord(n),
            ChordType::Extended(m) => m.make_chord(n),
        }
    }
}
impl Display for ChordType {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            ChordType::Triad(m) => Display::fmt(m, f),
            ChordType::Seventh(m) => Display::fmt(m, f),
            ChordType::Suspended(m) => Display::fmt(m, f),
            ChordType::Extended(m) => Display::fmt(m, f),
        }
    }
}
