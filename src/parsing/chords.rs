use core::str::FromStr;

use crate::{
    chord_type,
    chords::{Chord, ChordType},
    notes::MusicalNote,
    Mode,
};

use super::{parse_musical_note, Error};

pub fn parse_chord(chord: &str) -> Result<(MusicalNote, ChordType), Error> {
    let (start, tonality) = parse_musical_note(chord)?;
    let mode = match &chord[start..] {
        "" => Ok(chord_type::triad::Mode::Major.into()),
        "M" => Ok(chord_type::triad::Mode::Major.into()),
        "maj" => Ok(chord_type::triad::Mode::Major.into()),
        "Δ" => Ok(chord_type::triad::Mode::Major.into()),
        "ma" => Ok(chord_type::triad::Mode::Major.into()),
        "m" => Ok(chord_type::triad::Mode::Minor.into()),
        "min" => Ok(chord_type::triad::Mode::Minor.into()),
        "-" => Ok(chord_type::triad::Mode::Minor.into()),
        "mi" => Ok(chord_type::triad::Mode::Minor.into()),
        "aug" => Ok(chord_type::triad::Mode::Augmented.into()),
        "+" => Ok(chord_type::triad::Mode::Augmented.into()),
        "dim" => Ok(chord_type::triad::Mode::Diminished.into()),
        "°" => Ok(chord_type::triad::Mode::Diminished.into()),
        "m(♭5)" => Ok(chord_type::triad::Mode::Diminished.into()),
        "m(b5)" => Ok(chord_type::triad::Mode::Diminished.into()),
        "7" => Ok(chord_type::seventh::Mode::Dominant.into()),
        _ => Err(Error::UnknownMode),
    };
    Ok((tonality, mode?))
}

impl FromStr for Chord {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (t, m) = parse_chord(s)?;
        Ok(m.make_chord(t))
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        chord_type::{seventh, triad},
        chords::{Chord, ChordType},
        notes::{Accidental, MusicalNote, Note},
        parse_chord,
    };

    #[test]
    fn parse_ascii() {
        let result = &[
            [
                (Note::C, Accidental::Natural).into(),
                (Note::E, Accidental::Natural).into(),
                (Note::G, Accidental::Natural).into(),
            ],
            [
                (Note::C, Accidental::Natural).into(),
                (Note::E, Accidental::Flat).into(),
                (Note::G, Accidental::Natural).into(),
            ],
        ];
        for (note, expected) in ["C", "Cm"].iter().zip(result.iter()) {
            let parsed = note.parse::<Chord>();
            assert_eq!(parsed, Ok((*expected).into()));
        }
    }
    #[test]
    fn parse_chordpro() {
        for (s, expected) in &[
            ("C2", None),
            ("C3", None),
            ("C4", None),
            ("C5", None),
            ("C6", None),
            ("C69", None),
            (
                "C7",
                Some((
                    MusicalNote(Note::C, Accidental::Natural),
                    ChordType::Seventh(seventh::Mode::Dominant),
                )),
            ),
            ("C7-5", None),
            ("C7#5", None),
            ("C7#9", None),
            ("C7#9#5", None),
            ("C7#9b5", None),
            ("C7#9#11", None),
            ("C7b5", None),
            ("C7b9", None),
            ("C7b9#5", None),
            ("C7b9#9", None),
            ("C7b9#11", None),
            ("C7b9b13", None),
            ("C7b9b5", None),
            ("C7b9sus", None),
            ("C7b13", None),
            ("C7b13sus", None),
            ("C7-9", None),
            ("C7-9#11", None),
            ("C7-9#5", None),
            ("C7-9#9", None),
            ("C7-9-13", None),
            ("C7-9-5", None),
            ("C7-9sus", None),
            ("C711", None),
            ("C7#11", None),
            ("C7-13", None),
            ("C7-13sus", None),
            ("C7sus", None),
            ("C7susadd3", None),
            ("C7+", None),
            ("C7alt", None),
            ("C9", None),
            ("C9+", None),
            ("C9#5", None),
            ("C9b5", None),
            ("C9-5", None),
            ("C9sus", None),
            ("C9add6", None),
            ("Cmaj7", None),
            ("Cmaj711", None),
            ("Cmaj7#11", None),
            ("Cmaj13", None),
            ("Cmaj7#5", None),
            ("Cmaj7sus2", None),
            ("Cmaj7sus4", None),
            ("C^7", None),
            ("C^711", None),
            ("C^7#11", None),
            ("C^7#5", None),
            ("C^7sus2", None),
            ("C^7sus4", None),
            ("Cmaj9", None),
            ("Cmaj911", None),
            ("C^9", None),
            ("C^911", None),
            ("C^13", None),
            ("C^9#11", None),
            ("C11", None),
            ("C911", None),
            ("C9#11", None),
            ("C13", None),
            ("C13#11", None),
            ("C13#9", None),
            ("C13b9", None),
            ("Calt", None),
            ("Cadd2", None),
            ("Cadd4", None),
            ("Cadd9", None),
            ("Csus2", None),
            ("Csus4", None),
            ("Csus9", None),
            ("C6sus2", None),
            ("C6sus4", None),
            ("C7sus2", None),
            ("C7sus4", None),
            ("C13sus2", None),
            ("C13sus4", None),
            ("Cm#5", None),
            ("C-#5", None),
            ("Cm11", None),
            ("C-11", None),
            ("Cm6", None),
            ("C-6", None),
            ("Cm69", None),
            ("C-69", None),
            ("Cm7b5", None),
            ("C-7b5", None),
            ("Cm7-5", None),
            ("C-7-5", None),
            ("Cmmaj7", None),
            ("C-maj7", None),
            ("Cmmaj9", None),
            ("C-maj9", None),
            ("Cm9maj7", None),
            ("C-9maj7", None),
            ("Cm9^7", None),
            ("C-9^7", None),
            ("Cmadd9", None),
            ("C-add9", None),
            ("Cmb6", None),
            ("C-b6", None),
            ("Cm#7", None),
            ("C-#7", None),
            ("Cmsus4", None),
            ("Cmsus9", None),
            ("C-sus4", None),
            ("C-sus9", None),
            ("Cm7sus4", None),
            ("C-7sus4", None),
            (
                "C#aug",
                Some((
                    MusicalNote(Note::C, Accidental::Sharp),
                    ChordType::Triad(triad::Mode::Augmented),
                )),
            ),
            (
                "C+",
                Some((
                    MusicalNote(Note::C, Accidental::Natural),
                    ChordType::Triad(triad::Mode::Augmented),
                )),
            ),
            (
                "Cdim",
                Some((
                    MusicalNote(Note::C, Accidental::Natural),
                    ChordType::Triad(triad::Mode::Diminished),
                )),
            ),
            ("C0", None),
            ("Cdim7", None),
            ("Ch", None),
            ("Ch7", None),
            ("Ch9", None),
        ] {
            let parsed = parse_chord(s).ok();
            assert_eq!(parsed, *expected);
        }
    }
}
