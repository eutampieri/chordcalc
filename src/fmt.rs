use core::fmt::Display;

use crate::notes::{Accidental, MusicalNote, Note};
use naming_conventions::*;

pub mod naming_conventions {
    pub struct English<T>(pub T);
    pub struct German<T>(pub T);
    pub struct Swedish<T>(pub T);
    pub struct Dutch<T>(pub T);
    pub struct Romance<T>(pub T);
    pub struct Byzantine<T>(pub T);
    pub struct Japanese<T>(pub T);
    pub struct Hindustani<T>(pub T);
    pub struct Carnatic<T>(pub T);
    pub struct Bengali<T>(pub T);
}
impl Display for English<Note> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self.0 {
            Note::A => write!(f, "A"),
            Note::B => write!(f, "B"),
            Note::C => write!(f, "C"),
            Note::D => write!(f, "D"),
            Note::E => write!(f, "E"),
            Note::F => write!(f, "F"),
            Note::G => write!(f, "G"),
        }
    }
}
impl Display for Romance<Note> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self.0 {
            Note::A => write!(f, "La"),
            Note::B => write!(f, "Si"),
            Note::C => write!(f, "Do"),
            Note::D => write!(f, "Re"),
            Note::E => write!(f, "Mi"),
            Note::F => write!(f, "Fa"),
            Note::G => write!(f, "Sol"),
        }
    }
}
impl Display for Note {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        Display::fmt(&English(*self), f)
    }
}

impl Display for English<Accidental> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self.0 {
            Accidental::Sharp => write!(f, "♯"),
            Accidental::Flat => write!(f, "♭"),
            Accidental::Natural => write!(f, ""),
            Accidental::DoubleFlat => write!(f, "𝄫"),
            Accidental::DoubleSharp => write!(f, "𝄪"),
        }
    }
}
impl Display for Accidental {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        Display::fmt(&English(*self), f)
    }
}
impl Display for Romance<Accidental> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        Display::fmt(&self.0, f)
    }
}

impl Display for English<MusicalNote> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        Display::fmt(&English((self.0).0), f)?;
        Display::fmt(&English((self.0).1), f)?;
        Ok(())
    }
}

impl Display for MusicalNote {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        Display::fmt(&English(*self), f)
    }
}

impl Display for Romance<MusicalNote> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        Display::fmt(&Romance((self.0).0), f)?;
        Display::fmt(&Romance((self.0).1), f)?;
        Ok(())
    }
}
