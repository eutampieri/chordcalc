use core::fmt::Display;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Mode {
    DominantNinth,
    DominantEleventh,
    DominantThirteenth,
}

impl crate::Mode for Mode {
    fn make_chord(&self, tonality: crate::notes::MusicalNote) -> crate::chords::Chord {
        let root = tonality;
        match self {
            Mode::DominantNinth => [root, root + 4, root + 7, root + 10, root + 14].into(),
            Mode::DominantEleventh => {
                [root, root + 4, root + 7, root + 10, root + 14, root + 17].into()
            }
            Mode::DominantThirteenth => [
                root,
                root + 4,
                root + 7,
                root + 10,
                root + 14,
                root + 17,
                root + 21,
            ]
            .into(),
        }
    }
}
impl Display for Mode {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Mode::DominantNinth => write!(f, "⁹"),
            Mode::DominantEleventh => write!(f, "¹¹"),
            Mode::DominantThirteenth => write!(f, "¹³"),
        }
    }
}
