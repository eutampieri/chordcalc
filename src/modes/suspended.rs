use core::fmt::Display;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Mode {
    Second,
    Fourth,
    Jazz,
}

impl crate::Mode for Mode {
    fn make_chord(&self, tonality: crate::notes::MusicalNote) -> crate::chords::Chord {
        let root = tonality;
        match self {
            Mode::Second => [root, root + 2, root + 7].into(),
            Mode::Fourth => [root, root + 5, root + 7].into(),
            Mode::Jazz => [root, root + 5, root + 7, root + 10, root + 14].into(),
        }
    }
}
impl Display for Mode {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Mode::Second => write!(f, "sus2"),
            Mode::Fourth => write!(f, "sus4"),
            Mode::Jazz => write!(f, "9sus4"),
        }
    }
}
