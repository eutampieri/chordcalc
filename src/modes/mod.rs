use core::fmt::Display;

pub mod extended;
pub mod seventh;
pub mod suspended;
pub mod triad;

/// A Mode. From it you can make a chord
pub trait Mode: Display {
    /// Generate a chord in the specified tonality
    fn make_chord(&self, tonality: crate::notes::MusicalNote) -> crate::chords::Chord;
}
