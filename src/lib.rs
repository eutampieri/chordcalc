#![no_std]

/*
To have nice dbg!() and println!() in tests, enable this block
#[cfg(test)]
#[macro_use]
extern crate std;*/

pub mod chords;
pub mod fmt;
pub mod instruments;
mod modes;
pub mod notes;
mod parsing;

pub use parsing::parse_chord;
pub use parsing::Error as ParsingError;

/// Various chord modes
pub mod chord_type {
    pub mod triad {
        pub use crate::modes::triad::Mode;
    }
    pub mod seventh {
        pub use crate::modes::seventh::Mode;
    }
    pub mod extended {
        pub use crate::modes::extended::Mode;
    }
    pub mod suspended {
        pub use crate::modes::suspended::Mode;
    }
}
pub use modes::Mode;
